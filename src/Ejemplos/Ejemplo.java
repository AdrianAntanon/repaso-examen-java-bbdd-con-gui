package Ejemplos;

import java.sql.*;

public class Ejemplo {
    public static void main(String[] args) throws SQLException {

        String user = "adrian-antanyon-7e3";
        String key = "Contra123";
        String urlDatabase = "jdbc:postgresql://postgresql-adrian-antanyon-7e3.alwaysdata.net/adrian-antanyon-7e3_bbdd";
        //Class.forName("org.postgresql.Driver");

        Connection connection = null;

        try {
            connection = DriverManager.getConnection(urlDatabase, user, key);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        assert connection != null;
        Statement question = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

        ResultSet query = question.executeQuery("SELECT * FROM pf_productos");
        query.last(); // Vamos a la última fila
        query.relative(-2); // desde la última fila nos desplazamos dos hacia atrás
        query.beforeFirst(); // Nos desplazamos a la anterior a la primera, a la pseudofila donde empezamos
        query.next(); // Nos traslada a la primera fila
        query.next(); // Nos traslada a la fila dos porque avanza de uno en uno
        query.previous();  // Nos lleva a la fila anterior de donde estamos
        System.out.print(query.getString(1));
        System.out.println(" - " + query.getString(2));



    }
}
