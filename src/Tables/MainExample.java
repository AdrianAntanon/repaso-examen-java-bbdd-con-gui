package Tables;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class MainExample {
    public static void main(String[] args) {
        new Frame("Tabla de prueba");
    }
}

class Frame extends JFrame{
    public Frame(String title) throws HeadlessException {
        super(title);

        Panel myFirstTable = new Panel();

        add(myFirstTable);

        setBounds(800,800,800,800);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(Frame.EXIT_ON_CLOSE);
    }
}

class Panel extends JPanel{

    public Panel() {
        setLayout(new BorderLayout());

        String user = "adrian-antanyon-7e3";
        String key = "Contra123";
        String urlDatabase = "jdbc:postgresql://postgresql-adrian-antanyon-7e3.alwaysdata.net/adrian-antanyon-7e3_bbdd";




        try {
            //Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(urlDatabase, user, key);
            Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet resultSet = statement.executeQuery("SELECT * FROM pf_paises;");
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

//              Aquí cuenta las columnas y filas que tendrá mi tabla
            int columns = resultSetMetaData.getColumnCount();
            String [] columnsName = new String[columns];
            int rows = 0;

            while (resultSet.next()){
                rows++;
            }

//              Aquí consigo el nombre de las columnas
            for (int index = 1; index <= columns; index++) {
                String columnName = resultSetMetaData.getColumnLabel(index);
                columnsName[index-1] = columnName;
            }

//            Aquí restablezco mi cursor, es decir, lo devuelvo de la última posición a la primera y luego retrocedo un puesto para entrar en la pseudofila, es decir -1 y coger toda la información
            resultSet.first();
            resultSet.previous();


//              Aquí saco la información que introduciré en la tabla
            String [][] data = new String[rows][columns];

            int index = 0;
            while (resultSet.next()) {
                String productName = resultSet.getString(1);
                String countryName = resultSet.getString(2);

                data[index][0] = productName;
                data[index][1] = countryName;

                index++;
            }

//            Creo las tabla por default
            DefaultTableModel tableModel = new DefaultTableModel(data, columnsName);



//            Añadimos un botón para añadir

            JButton button = new JButton("Add");

            int finalRows = rows;
            button.addActionListener(e -> {
//                Aquí consigo el id del último país, data es un vector donde guardo los datos, y hago referencia a las filas que tiene - 1 para acceder a la última posición, dentro de ella a la posición 0 que es donde guardo el id
                System.out.println(data[finalRows -1][0]);
                int nextId = Integer.parseInt(data[finalRows -1][0]) + 1;
                try {
//                    Creo el objecto PreparedStatement para realizar consultas DML (Update y Delete)
                    PreparedStatement preparedStatement = connection.prepareStatement( "INSERT INTO pf_paises VALUES(?, ?);");
//                    Relleno la información para el update
                    preparedStatement.setString(1, String.valueOf(nextId));
                    preparedStatement.setString(2, "Japón");
//                    Ejecuto la sentencia
                    preparedStatement.executeQuery();
//                    Lo añado a la tabla
                    String [] updateTable = {nextId+"", "Japón"};
                    tableModel.addRow(updateTable);

//                    NO FUNCIONA!!!! SOLO ESTA PARTE

                }catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            });

            add(button, BorderLayout.SOUTH);

//              Creo las tablas y las relleno con los datos obtenidos previamente e introducidos en tableModel
            JTable table = new JTable(tableModel);


//             Lo añado al ScrollPane si no no podré usar la rueda para bajar y subir
            JScrollPane tableView = new JScrollPane(table);

//            Por último lo añado a mi panel
            add(tableView);

//              Cierro conexión la bbdd
//            connection.close();
//            statement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }



    }
}
